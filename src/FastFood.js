import React, { Component } from 'react'
import FoodList from './components/food-list/FoodList'
import OrderDetails from "./components/order-details/OrderDetails";

class FastFood extends Component {
  render() {
    return (
      <div className="food-menu">
        <FoodList/>
        <OrderDetails/>
      </div>
    )
  }
}

export default FastFood