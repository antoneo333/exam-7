import React from 'react';
import './App.css';
// import FoodCard from 'components/food-card/FoodCard'
import FastFood from './FastFood'

function App() {
  return (
    <div className="App">
      <FastFood/>
    </div>
  );
}

export default App;
